<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style: none;
        }
    </style>
</head>

<body>
    <ul>
        <?php
        // Calcular la tabla de multiplicar del 2
        $numero = 2;
        $maximo = 20;

        for ($i = 1; $i <= $maximo; $i++) {

        ?>
            <li> <?= "$i*$numero = " . $numero * $i ?> </li>

        <?php
        }

        ?>
    </ul>
</body>

</html>