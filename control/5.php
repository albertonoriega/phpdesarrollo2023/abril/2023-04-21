<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style: none;
        }
    </style>
</head>

<body>

    <form action="">
        <div>
            <label for="numero">Numero del que quieres saber la tabla de multiplicar</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <label for="interacciones">Número hasta el que quieres que multiplique</label>
            <input type="number" name="interacciones" id="interacciones">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <ul>
        <?php
        if (isset($_GET["enviar"])) {

            $numero = $_GET["numero"];
            $maximo = $_GET["interacciones"];


            for ($i = 1; $i <= $maximo; $i++) {

        ?>
                <li> <?= "$i*$numero = " . $numero * $i ?> </li>

        <?php
            }
        }
        ?>
    </ul>
</body>

</html>