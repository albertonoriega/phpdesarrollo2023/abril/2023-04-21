<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $numero = 10;

    for ($i = 1; $i <= 10; $i++) {
        echo "$numero*$i = " .  ($numero * $i) . "<br>";
    }

    for ($i = 1; $i <= 10; $i++) {
    ?>
        <br> <?= "{$numero}*{$i} = " . ($numero * $i) ?>

    <?php
    }
    ?>
</body>

</html>