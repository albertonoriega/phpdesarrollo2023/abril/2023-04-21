<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        rect {
            stroke-width: 3;
            stroke: black;
        }

        rect.rojo {
            fill: red;
        }

        rect.azul {
            fill: blue;
        }

        rect.rosa {
            fill: pink;
        }
    </style>
</head>

<body>

    <svg width="800" height="800">
        <?php
        // Fibujamos 10 rectágulos con svg
        $numero = 10;
        for ($i = 1; $i <= $numero; $i++) {
        ?>

            <rect class="rojo" x="<?= 10 + 60 * ($i - 1) ?>" y="10" width="50" height="50" />
            <rect class="rojo" x="<?= 10 + 60 * ($i - 1) ?>" y="550" width="50" height="50" />
            <rect class="azul" x="10" y="<?= 10 + 60 * ($i - 1) ?>" width="50" height="50" />
            <rect class="azul" x="550" y="<?= 10 + 60 * ($i - 1) ?>" width="50" height="50" />
            <rect class="rosa" x="<?= 10 + 60 * ($i - 1) ?>" y="<?= 10 + 60 * ($i - 1) ?>" width="50" height="50" />
            <rect class="rosa" x="<?= 10 + 60 * ($i - 1) ?>" y="<?= 550 - 60 * ($i - 1) ?>" width="50" height="50" />
        <?php
        }
        ?>
    </svg>

</body>

</html>